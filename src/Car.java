package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;
import java.io.IOException;

public class Car extends JPanel implements Runnable{
	private Road road;
	private Image carimage, carupimage, cardownimage;
	private int pos, vert, direction;
	private int v, newv;
	private int speedreaction, lanereaction;
	private boolean end;
	
	private void changespeed(){
		if(speedreaction > 0){
			speedreaction--;
		}
		else{
			if(newv > v){
				v++;
			}
			else if(newv < v){
				v--;
			}
			newv = road.getFrontdistance(this)/2;
			if(newv > v){
				speedreaction = 100;
			}
			else if(newv < v){
				speedreaction = 50;
			}
		}
		
		if(vert != 20 && vert != 70){
			if(direction == 1){
				vert--;
			}
			if(direction == 2){
				vert++;
			}
			if(vert == 20 || vert == 70)
				lanereaction = 50;
		}
		else if(lanereaction > 0){
			lanereaction--;
		}
		else if(road.getFrontdistance(this) < 5){
			if(vert == 70)
				vert--;
			else if(vert == 20)
				vert++;
		}
	}
	
	private void move(){
		pos += v;
	}
	
	public Car(Road road, Image carimage, Image carupimage, Image cardownimage, int lane){
		this.setLayout(null);
		this.setOpaque(false);
		this.carimage = carimage;
		this.carupimage = carupimage;
		this.cardownimage = cardownimage;
		this.road = road;
		if(lane == 1){
			vert = 70;
			direction = 1;
		}
		else if(lane == 2){
			vert = 20;
			direction = 2;
		}
		pos = 0;
		speedreaction = 0;
		lanereaction = 50;
	}
	
	public void run(){
		v = road.getFrontdistance(this)/2;
		newv = v;
		while(pos < 1800 && !end){
			try{
				Thread.sleep(10);
			}
			catch(InterruptedException ex){
			}
			changespeed();
			move();
		}
	}
	
	public int getPos(){
		return pos;
	}
	
	public int getVert(){
		return vert;
	}
	
	public void setEnd(){
		end = true;
	}
	
	protected void paintComponent(Graphics g) {
        super.paintComponent(g);
		if(vert == 20 || vert == 70){
			g.drawImage(carimage, pos, vert, this);
		}
		else if(direction == 1){
			g.drawImage(carupimage, pos, vert, this);
		}
		else if(direction == 2){
			g.drawImage(cardownimage, pos, vert, this);
		}
    }
}