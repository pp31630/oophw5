package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;
 
public class JavaApplet extends JApplet{
	private Road road;
	private MyButton mode, addLower, addUpper, restart;
	private Image roadimage = null, carimage, carupimage, cardownimage, explodeimage;
	private boolean auto = true;
	
	public void init(){
		setVisible(true);
		getContentPane().setLayout(null);
		roadimage = getImage(getDocumentBase(), "img/road.jpg");
		carimage = getImage(getDocumentBase(), "img/car.png");
		carupimage = getImage(getDocumentBase(), "img/carup.png");
		cardownimage = getImage(getDocumentBase(), "img/cardown.png");
		explodeimage = getImage(getDocumentBase(), "img/explosion.png");
		road = new Road(roadimage, carimage, carupimage, cardownimage, explodeimage);
		getContentPane().add(road);
		road.setBounds(0, 0, 1800, 125);
		mode = new MyButton(this, road, "manual");
		getContentPane().add(mode);
		mode.setBounds(0, 125, 180, 25);
		addLower = new MyButton(this, road, "Add Car To Lower Lane");
		getContentPane().add(addLower);
		addLower.setBounds(180, 125, 180, 25);
		addUpper = new MyButton(this, road, "Add Car To Upper Lane");
		getContentPane().add(addUpper);
		addUpper.setBounds(360, 125, 180, 25);
		restart = new MyButton(this, road, "restart");
		getContentPane().add(restart);
		restart.setBounds(540, 125, 180, 25);
	}
	
	public void start(){
		while(true){
			if(auto && road.getSpace() > 4){
				road.create(1);
			}
			getContentPane().repaint();
		}
	}
	
	public boolean getMode(){
		return auto;
	}
	
	public void setMode(boolean mode){
		this.auto = mode;
	}
}