package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;

public class MyButton extends JButton{
	private Road road;

    public MyButton(JavaApplet control, Road road, String name){
		this.setLayout(null);
		this.road = road;
		setText(name);
		setActionCommand(name);
		if(name.equals("manual")){
			addActionListener(new ButtonListener(control, road, this));
		}
		else{
			addActionListener(new ButtonListener(control, road));
		}
    }
}

