package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;

public class Explosion extends JPanel{
	private Image explodeimage;
	private int pos, vert;

	public Explosion(Image explodeimage, int pos, int vert){
		this.setLayout(null);
		this.setOpaque(false);
		this.explodeimage = explodeimage;
		this.pos = pos;
		this.vert = vert;
	}

    protected void paintComponent(Graphics g){
        super.paintComponent(g);
		g.drawImage(explodeimage, pos, vert-30, this);
    }
}