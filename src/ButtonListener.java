package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class ButtonListener implements ActionListener{
	private JavaApplet control;
	private Road road;
	private MyButton modebutton;

    public ButtonListener(JavaApplet control, Road road){
		this.control = control;
		this.road = road;
    }
	
	public ButtonListener(JavaApplet control, Road road, MyButton modebutton){
		this.control = control;
		this.road = road;
		this.modebutton = modebutton;
    }
	
    public void actionPerformed(ActionEvent e){
		String cmd = e.getActionCommand();
		if(cmd.equals("manual")){
			if(control.getMode()){
				modebutton.setText("auto");
				control.setMode(false);
			}
			else{
				modebutton.setText("manual");
				control.setMode(true);
			}
		}
		else if(cmd.equals("Add Car To Lower Lane") && !control.getMode()){
			road.create(1);
		}
		else if(cmd.equals("Add Car To Upper Lane") && !control.getMode()){
			road.create(2);
		}
		else if(cmd.equals("restart")){
			road.refresh();
		}
    }
}