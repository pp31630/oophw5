package ntu.csie.oop13spring;

import java.awt.*;
import javax.swing.*;
import java.lang.Math;

public class Road extends JPanel{
	private static int MAXCAR = 100;
	private Thread[] t = new Thread[MAXCAR];
	private Car[] car = new Car[MAXCAR];
	private Explosion exp;
	private Image roadimage, carimage, carupimage, cardownimage, explodeimage;
	private boolean on = true;
	
	public Road(Image roadimage, Image carimage, Image carupimage, Image cardownimage, Image explodeimage){
		this.setLayout(null);
		this.roadimage = roadimage;
		this.carimage = carimage;
		this.carupimage = carupimage;
		this.cardownimage = cardownimage;
		this.explodeimage = explodeimage;
	}

	public void create(int lane){
		System.out.println("create car");
		for(int i = 0; i < MAXCAR; i++)
			if(car[i] == null && on){
				car[i] = new Car(this, carimage, carupimage, cardownimage, lane);
				add(car[i]);
				car[i].setBounds(0, 0, 1800, 125);
				t[i] = new Thread(car[i]);
				t[i].start();
				break;
			}
	}
	
	public void refresh(){
		for(int i = 0; i < MAXCAR; i++)
			if(car[i] != null){
				car[i].setEnd();
				remove(car[i]);
				car[i] = null;
			}
		if(exp != null){
			remove(exp);
			exp = null;
		}
		on = true;
	}
	
	public synchronized int getFrontdistance(Car c){
		int p = c.getPos();
		int v = c.getVert();
		int tmpp, tmpv;
		int frontp = 2100;
		
		for(int i = 0; i < MAXCAR; i++){
			if(car[i] != null && car[i].getPos() >= 1800){
				remove(car[i]);
				car[i] = null;
			}
			if(car[i] != null){
				tmpp = car[i].getPos();
				tmpv = car[i].getVert();
				if(Math.abs(tmpv-v) < 20 && tmpp > p && tmpp < frontp)
					frontp = tmpp;
			}
		}
		
		if(p < 1800 && frontp-p < 70 && on){
			System.out.println("Crash");
			if(exp != null){
				remove(exp);
			}
			exp = new Explosion(explodeimage, (frontp+p)/2, v);
			add(exp);
			exp.setBounds(0, 0, 1800, 125);
			for(int i = 0; i < MAXCAR; i++)
				if(car[i] != null)
					car[i].setEnd();
			on = false;
		}

		return ((frontp-p)/30 > 8) ? 8 : ((frontp-p)/30);
	}
	
	public int getSpace(){
		int min = 1800;
		for(int i = 0; i < MAXCAR; i++)
			if(car[i] != null && car[i].getVert() == 70 && car[i].getPos() < min){
				min = car[i].getPos();
			}
		return min/30;
	}
	
	protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(roadimage, 0, 0, this);
    }
}